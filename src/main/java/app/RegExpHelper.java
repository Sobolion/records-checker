package app;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpHelper {

    public static Boolean matches(String text, String regexp) {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(text);
        return matcher.find();
    }

}
